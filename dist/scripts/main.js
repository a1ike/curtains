'use strict';

$(document).ready(function () {

  $('.c-gallery__for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows: false,
    fade: true,
    asNavFor: '.c-gallery__nav'
  });

  $('.c-gallery__nav').slick({
    slidesToShow: 8,
    slidesToScroll: 1,
    asNavFor: '.c-gallery__for',
    dots: false,
    arrows: true,
    centerMode: false,
    focusOnSelect: true,
    responsive: [{
      breakpoint: 1199,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }]
  });

  $('.c-companies__for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows: false,
    fade: true,
    asNavFor: '.c-companies__nav'
  });

  $('.c-companies__nav').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.c-companies__for',
    dots: false,
    arrows: true,
    centerMode: false,
    focusOnSelect: true,
    responsive: [{
      breakpoint: 1199,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
  });

  $('.phone').inputmask('+7(999)999-99-99');

  $('.c-stepper__toggler').on('click', function (e) {
    e.preventDefault();

    // Remove active class from buttons
    $('.c-stepper__toggler').removeClass('active');

    // Add the correct active class
    $(this).addClass('active');

    // Hide all tab items
    $('[class^=tab-item]').hide();

    // Show the clicked tab
    var num = $(this).data('tab-item');
    $('.tab-item-' + num).fadeIn();
  });

  $('.c-collect__button').on('click', function (e) {
    e.preventDefault();

    $('.c-collect__flex').slideToggle('fast', function (e) {
      // callback
    });
    $('.c-stepper__wrapper').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.c-stepper__close').on('click', function (e) {
    e.preventDefault();

    $('.c-collect__flex').slideToggle('fast', function (e) {
      // callback
    });
    $('.c-stepper__wrapper').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('input[name="amount"]').on('input', function () {
    if ($(this).val() !== '') {
      $('#validateAmount').removeClass('not-active');
    } else {
      $('#validateAmount').addClass('not-active');
    }
  });

  $('input[name="phone"]').on('input', function () {
    if ($(this).inputmask('isComplete')) {
      $('#validatePhone').removeClass('not-active');
    } else {
      $('#validatePhone').addClass('not-active');
    }
  });

  $('#form1').submit(function (e) {
    e.preventDefault();
    var f = $(this);
    $('.ierror', f).removeClass('ierror');
    var goods = $('input[name="goods"]', f).val();
    var types = $('input[name="types"]', f).val();
    var sizes = $('input[name="sizes"]', f).val();
    var amount = $('input[name="amount"]', f).val();
    var phone = $('input[name="phone"]', f).val();
    var error = false;
    if (error) {
      return false;
    }
    $.ajax({
      type: 'POST',
      url: 'mail.php',
      data: $(this).serialize()
    }).done(function (data) {
      if (data.response == 'ok') {
        $('.c-collect__flex').slideToggle('fast', function (e) {
          // callback
        });
        $('.c-stepper__wrapper').slideToggle('fast', function (e) {
          // callback
        });
        window.location.replace('order.php');
      } else {
        alert('Ошибка, попробуйте повторить позже!');
      }
    });
    return false;
  });

  $('#form2').submit(function (e) {
    e.preventDefault();
    var f = $(this);
    $('.ierror', f).removeClass('ierror');
    var person = $('input[name="person"]', f).val();
    var phone = $('input[name="phone"]', f).val();
    var email = $('input[name="email"]', f).val();
    var error = false;
    if (person == '') {
      $('input[name="person"]', f).addClass('ierror');
      error = true;
    }
    if (phone == '') {
      $('input[name="phone"]', f).addClass('ierror');
      error = true;
    }
    if (error) {
      return false;
    }
    $.ajax({
      type: 'POST',
      url: 'mail.php',
      data: $(this).serialize()
    }).done(function (data) {
      if (data.response == 'ok') {
        window.location.replace('order.php');
      } else {
        alert('Ошибка, попробуйте повторить позже!');
      }
    });
    return false;
  });

  $('#form3').submit(function (e) {
    e.preventDefault();
    var f = $(this);
    $('.ierror', f).removeClass('ierror');
    var person = $('input[name="person"]', f).val();
    var phone = $('input[name="phone"]', f).val();
    var error = false;
    if (person == '') {
      $('input[name="person"]', f).addClass('ierror');
      error = true;
    }
    if (phone == '') {
      $('input[name="phone"]', f).addClass('ierror');
      error = true;
    }
    if (error) {
      return false;
    }
    $.ajax({
      type: 'POST',
      url: 'mail.php',
      data: $(this).serialize()
    }).done(function (data) {
      if (data.response == 'ok') {
        window.location.replace('order.php');
      } else {
        alert('Ошибка, попробуйте повторить позже!');
      }
    });
    return false;
  });
});